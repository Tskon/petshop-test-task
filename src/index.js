import 'scss/main.scss';
import Vue from 'vue/dist/vue.esm.js';
import Datepicker from 'vuejs-datepicker';
import { ru } from 'vuejs-datepicker/dist/locale'

new Vue({
  el: '#app',
  data: {
    date: new Date(),
    isNeedHighligting: false,
    dataPickerLang: ru,
  },
  computed: {
    highlighted() {
      return {
        customPredictor: (date) => {
          if(this.isNeedHighligting){
            const cellData = {
              dayOfWeek: date.getDay(),
              dayOfMonth: date.getDate(),
              Month: date.getMonth()
            };
            const selectedData = {
              dayOfWeek: this.date.getDay(),
              dayOfMonth: this.date.getDate(),
              Month: this.date.getMonth()
            };
            return cellData.dayOfWeek === selectedData.dayOfWeek &&
              cellData.dayOfMonth > selectedData.dayOfMonth && cellData.Month === selectedData.Month;
          }
        }
      }
    }
  },
  methods: {
    customFormatter(date) {
      return date.toLocaleString('ru', { weekday: 'long' });
    },
    turnOnHighlighting(){
      this.isNeedHighligting = true;
    }
  },
  components: {
    Datepicker
  },
  template: `
<main>
  <div class="dataPicker">
    <datepicker 
    :value="date"
    v-model="date"
    input-class="dataPicker__input"
    :language="dataPickerLang"
    :format="customFormatter"
    :highlighted="highlighted"
    :monday-first="true"
    @selected="turnOnHighlighting"
    ></datepicker>
  </div>
</main>
  `
});